const express = require('express');
const Foodtruck = require('../models/foodtruck');

const ftRouter = express.Router();

ftRouter.get('/all', (req, res) => {
    Foodtruck.find({}, (err, foodtrucks) => {
        if (err) console.error(err)
        res.render('foodtruck', {title: 'Foodtrucks', foodtrucks: foodtrucks})
    })
});

ftRouter.route('/add')
    .get((req, res) => {
        res.render('add')
    })
    .post((req, res) => {
    const newFoodtruck = new Foodtruck(req.body);
    newFoodtruck.save((err, foodtruck) => {
        if (err) console.error(err)
        res.redirect('/foodtrucks/all');
    })
})

ftRouter.get('/remove/:id', (req, res) => {
    Foodtruck.deleteOne({_id: req.params.id}, (err, foodtruck)=>{
        if (err) console.error(err)
        res.redirect('/foodtrucks/all')
    })
});

ftRouter.route('/edit/:id')
    .get((req, res) => {
        Foodtruck.findById(req.params.id, (err, foodtruck) => {
            if (err) console.error(err)
            res.render('edit', {foodtruck: foodtruck});
        })
    })
    .post((req,res) => {
        Foodtruck.findById({_id: req.params.id}, (err, foodtruck) => {
            if (err) console.error(err)
            Object.assign(foodtruck, req.body).save((err, foodtruck)=>{
                if (err) console.error(err)
                res.redirect('/foodtrucks/all');
            })
        })
    })

module.exports = ftRouter;