const express = require('express');
const router = express.Router();
const basciRouter = require('./basicRoute');
const ftRouter = require('./foodtruckRoute');

router.use('/', basciRouter);
router.use('/foodTrucks', ftRouter);

module.exports = router;