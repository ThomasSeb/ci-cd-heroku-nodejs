const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const foodTruckSchema = new Schema ({
    name: { type: String, required: true },
    foodType: { type: String, required: true}
});

const Foodtruck = mongoose.model('Foodtruck', foodTruckSchema);

module.exports = Foodtruck;