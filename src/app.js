const express = require('express');
const app = express();
const volleyball = require('volleyball');
const path = require('path');
const mongoose = require('mongoose');

//mongoose.connect('mongodb+srv://liridona:liridonaisufisimplon@liridona-w2cda.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true });
const db = process.env.MONGODB_URL;

const connectDB = async () => {
    try {
        await mongoose.connect(db, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        });
        console.log("MongoDB is Connected...");
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};
connectDB()

const router = require('./routes/index');


require('dotenv').config()

const { DB_PORT } = process.env;
const port = process.env.PORT || DB_PORT

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(volleyball);
app.set('view engine', 'pug');
app.set("views", path.join(__dirname, "views"));
app.use(express.static('public'));

app.use('/', router);

app.listen(port, () => {
    console.log(`[Server run at: http://localhost:${port}]`)
});